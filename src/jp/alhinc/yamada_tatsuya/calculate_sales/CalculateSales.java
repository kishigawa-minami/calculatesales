package jp.alhinc.yamada_tatsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> branchMap = new HashMap<String, String>();	//支店定義のマップ
		Map<String, Long> salesMap = new HashMap<String, Long>();			//売上のマップ

		//支店定義ファイルの読み込み
		BufferedReader br = null;
		FileReader fr = null;
		try {
			File storeFile = new File(args[0], "branch.lst");
			fr = new FileReader(storeFile);
			br = new BufferedReader(fr);

			//支店定義ファイルを一行ずつ読み込み
			String line;
			while((line = br.readLine()) != null) {

				//支店定義ファイルのフォーマットが不正な場合のエラー処理
				if(!(line.matches("^[0-9]{3},[^,]+"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//文字列の分割
				String[] storeCode = line.split(",");

				//支店コードに対応する支店名を保持（途中）
				branchMap.put(storeCode[0], storeCode[1]);
				salesMap.put(storeCode[0], 0L);
			}

			//支店定義ファイルが存在しないときのエラー処理
		} catch (FileNotFoundException e) {
			System.out.println("支店定義ファイルが存在しません");
			return;

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		}finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//該当ファイルを検索(2番の処理)
		//検索用のフィルターを作る
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if(str.matches("[0-9]{8}\\.(rcd)$") && new File(file, str).isFile()) {
					return true;
				} else {
					return false;
				}
			}
		};

		//フィルターをかけてファイルを検索
		File[] list = new File(args[0]).listFiles(filter);

		//売上ファイル連番でなければエラー処理を入れる（途中）
		int checkNum = Integer.parseInt(list[0].getName().substring(0,8));
		for(int plasNum = 0; plasNum < list.length; plasNum++) {
			if(Integer.parseInt(list[plasNum].getName().substring(0,8)) != checkNum + plasNum) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売上ファイルの読み込み
		for(int i = 0; i < list.length; i++) {
			br = null;

			try {
				fr = new FileReader(list[i]);
				br = new BufferedReader(fr);

				//売上額を加算する
				String salesAmount = null;
				Integer count = 0;

				//売上ファイルの中身が3行以上ある場合のエラー処理
				while((salesAmount = br.readLine()) != null) {
					count++;

					if(branchMap.containsKey(salesAmount)) {
						long sales = Long.parseLong(br.readLine());
						count++;
						salesMap.put(salesAmount, salesMap.get(salesAmount) + sales);
					} else if(count > 2){
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						return;
					} else  {					//支店に該当がない場合のエラー処理
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}

					//合計金額10ケタ超えでエラー処理
					if(10 < String.valueOf(salesMap.get(salesAmount)).length()) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}

				BufferedWriter bw = null;

				//ファイルの出力(3番の処理)
				File aggregateFile = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(aggregateFile);
				bw = new BufferedWriter(fw);
				for(String storeCode : salesMap.keySet()) {
					bw.write(storeCode + "," + branchMap.get(storeCode) + "," + salesMap.get(storeCode));
					bw.newLine();
				}
				bw.close();

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
	}
}